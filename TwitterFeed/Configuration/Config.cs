﻿namespace TwitterFeed.Configuration
{
    public static class Config
    {
        public const string UserFile = "user.txt";
        public const string TweetFile = "tweet.txt";
        public const string pathName = "assets";
    }
}
