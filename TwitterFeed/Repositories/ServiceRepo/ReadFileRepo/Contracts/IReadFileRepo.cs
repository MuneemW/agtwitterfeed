﻿using System.Collections.Generic;

namespace TwitterFeed.Repositories.ServiceRepo.ReadFileRepo.Contracts
{
    /// <summary>
    /// This interface is used to manage twitter files
    /// </summary>
    public interface IReadFileRepo
    {
        /// <summary>
        /// This method reads the tweet file
        /// </summary>
        /// <param name="filepath">The filePath to read</param>
        /// <returns>The file's data</returns>
        Dictionary<string, List<string>> ReadTweetFile(string filepath);

        /// <summary>
        /// This method reads the user file
        /// </summary>
        /// <param name="filepath">The filePath to read</param>
        /// <returns>The file's data</returns>
        Dictionary<string, List<string>> ReadUserFile(string filepath);
    }
}
