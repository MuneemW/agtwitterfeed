﻿using System.Collections.Generic;
using System.Text;
using TwitterFeed.Models;

namespace TwitterFeed.Repositories.ServiceRepo.PrintFeedRepo.Contracts
{
    /// <summary>
    /// This Interface is used to manage Feeds
    /// </summary>
    public interface IPrintFeed
    {
        /// <summary>
        /// This message builds and returns a twitter feed
        /// </summary>
        /// <param name="follower">List of user followers</param>
        /// <param name="messsage">List of messages to print</param>
        /// <returns>A formatted string with the follower and tweet message</returns>
        StringBuilder PrintTweets(List<Follower> follower, List<Message> messsage);
    }
}
